<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => '/auth'], function () {
    Route::post('/register', ['\App\Http\Controllers\Api\AuthController', 'register']);
    Route::post('/login', ['\App\Http\Controllers\Api\AuthController', 'login']);
});


Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('products', 'App\Http\Controllers\Api\ProductController');
    Route::put('/user/{user}', ['\App\Http\Controllers\Api\UserController', 'update']);
});
